<!--
SPDX-License-Identifier: BSD-2-Clause-Patent
Copyright (c) 2020 the prplMesh contributors
This code is subject to the terms of the BSD+Patent license.
See LICENSE file for more details.
-->

# RDKB (OE 3.1)

## HOWTO build

### Fetch sources

* Fetch RDKB

If build Turris Omnia then

```bash
repo init -u https://code.rdkcentral.com/r/manifests -b rdkb-2021q1-dunfell -m rdkb-turris-extsrc.xml
repo sync -j$(nproc) --no-clone-bundle
```
If build RaspberryPi 3B+ then

```bash
repo init -u https://code.rdkcentral.com/r/manifests -b rdkb-2021q1-dunfell -m rdkb-raspberrypi.xml
repo sync -j$(nproc) --no-clone-bundle
```

* Fetch meta-prplmesh
```bash
git clone https://gitlab.com/prpl-foundation/prplmesh/meta-prplmesh.git
```

* Fetch Ambiorix meta layers
```bash
git clone https://gitlab.com/soft.at.home/buildsystems/yocto/meta-amx.git
git clone https://gitlab.com/soft.at.home/buildsystems/yocto/meta-componentlst.git
```

* Switch them to commits with tested build
```bash
(cd meta-amx          && git checkout afd3b1cc354071cd5fc98e0526599d63aad422ef)
(cd meta-componentlst && git checkout 64806744d41497da950f188568fa327ebccd022a)
```

### Source build env

If build for TurrisOmnia then

```bash
MACHINE=turris source meta-turris/setup-environment
```

If build RaspberryPi 3B+ then

```bash
MACHINE=raspberrypi-rdk-broadband source meta-cmf-raspberrypi/setup-environment
```

### Append meta layers

Append Ambiorix layer in file `conf/bblayers.conf`
```bash
BBLAYERS += "${RDKROOT}/meta-amx"
```

* Append meta-prplmesh layer in file `conf/bblayers.conf`
```
BBLAYERS += "${RDKROOT}/meta-prplmesh"
```

### Run image build

```
bitbake rdk-generic-broadband-image
```

## HOWTO build with MMX

Follow manual above but make additional steps ...

* ... on fetch sources
** Fetch meta layers required to MMX following [meta-mmx/README.md](https://github.com/InangoSystems/meta-mmx/blob/master/README.md)
** Apply patches to Ambiorix meta layers from folder "meta-prplmesh/patches"
* ... after source build env
** Append meta layers required to MMX following "meta-mmx/README.md"

## HOWTO flash image

To flash image following official manual: 
* for Turris Omnia: https://wiki.rdkcentral.com/pages/viewpage.action?pageId=114986660
* for RaspberryPi 3B+: https://wiki.rdkcentral.com/pages/viewpage.action?pageId=130090892 

## HOWTO run prplMesh

* Open board UART

* Check that U-Bus daemon run
```bash
ps aux | grep ubusd
```

* Check that hostapd daemon run
```bash
ps aux | grep hostapd
```

* And check that prplMesh run
```bash
/opt/prplmesh/scripts/prplmesh_utils.sh status
# you should see that prplMesh controller and agent(s) are operational

ubus list
# you should see Ambiorix objects
```

## HOWTO run MMX

* Run uci-defaults script
```
/etc/uci-defaults/luci-theme-openwrt-inango-prplmesh-en
```

Following [meta-mmx/README.md](https://github.com/InangoSystems/meta-mmx/blob/master/README.md) to run MMX components
