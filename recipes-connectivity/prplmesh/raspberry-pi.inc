# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
#
# RPi machine specific
#
# 2nd WLAN iface is not active on RPi
# It's needed to plug-in Wi-Fi USB dongle to have two wlan ifaces:
#    wlan0 and wlan1
BEEROCKS_WLAN1_IFACE_raspberrypi-rdk-broadband = "wlan0"
BEEROCKS_WLAN2_IFACE_raspberrypi-rdk-broadband = "wlan1"
BEEROCKS_HOSTAP_WLAN1_CTRL_IFACE_raspberrypi-rdk-broadband = "/var/run/hostapd0/wlan0"
BEEROCKS_HOSTAP_WLAN2_CTRL_IFACE_raspberrypi-rdk-broadband = "/var/run/hostapd4/wlan1"
