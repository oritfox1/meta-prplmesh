# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 
# the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

inherit cmake pythonnative

S = "${WORKDIR}/git"
OECMAKE_SOURCEPATH = "${S}/"

SUMMARY = "prplMesh"
SECTION = "net"
LICENSE = "BSD-2-Clause-Patent.txt"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0518d409dae93098cca8dfa932f3ab1b"

DEPENDS = "python-native python-pyyaml-native json-c openssl readline"
RDEPENDS_${PN} = "iproute2 busybox"

SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/prpl-foundation/prplmesh/prplMesh.git;protocol=https;branch=stable/${PV}"

PACKAGECONFIG ??= "${BWL_TYPE} nbapi"

TARGET_PLATFORM ?= "linux"
BWL_TYPE ?= "bwl-nl80211"
INSTALL_PREFIX ?= "/opt/prplmesh"
#
# RDKB specific
#
BEEROCKS_BRIDGE_IFACE_rdk ?= "brlan0"
BEEROCKS_BH_WIRE_IFACE_rdk ?= "erouter0"

BEEROCKS_LOG_FILES_ENABLED_rdk ?= "true"
BEEROCKS_LOG_SYSLOG_ENABLED_rdk ?= "false"
BEEROCKS_LOG_FILES_AUTO_ROLL_rdk ?= "false"
BEEROCKS_LOG_FILES_PATH_rdk ?= "/rdklogs/logs"
BEEROCKS_LOG_FILES_SUFFIX_rdk = ".log"


PACKAGECONFIG[release] = "-DCMAKE_BUILD_TYPE=Release,-DCMAKE_BUILD_TYPE=Debug,,"

# Add explicit linking beerocks bwl lib with wpa_client lib
# When OpenWrt toolchain build prplMesh the linker use object file
# "wpa_ctrl.c.o" directly from hostapd internal build folder
# But it's not possible in Yocto environment so add dependency on
# "wpa-supplicant" recipe to use "wpa_client" library which it builds
PACKAGECONFIG[bwl-nl80211] = "-DBWL_TYPE=NL80211 -DBWL_LIBS=wpa_client,,wpa-supplicant libnl,wpa-supplicant hostapd"
PACKAGECONFIG[bwl-dummy] = "-DBWL_TYPE=DUMMY,,wpa-supplicant libnl,wpa-supplicant"
PACKAGECONFIG[bwl-dwpal] = "-DBWL_TYPE=DWPAL,,libnl ubus uci wav-dpal,"
PACKAGECONFIG[nbapi] = "-DENABLE_NBAPI:BOOL=ON,\
                        -DENABLE_NBAPI:BOOL=OFF,\
                        libamxb libamxc libamxd libamxj libamxo libamxp amxb-inspect amxo-cg mod-amxb-ubus,\
                        ubus ubusd mod-amxb-ubus"

PACKAGECONFIG[mmx] = ",,,packagegroup-mmx"

# Redefinition of next cmake variables
#   CMAKE_INSTALL_INCLUDEDIR
#   CMAKE_INSTALL_LIBDIR
# is needed to make -dbg/-dev packages containing headers/libraries in
# system folders:
#   /usr/include/**/*.h
#   /usr/lib/**/*.so
# Without redefinition they will be located at
#   /opt/prplmesh/include/**/*.h
#   /opt/prplmesh/lib/**/*.so
EXTRA_OECMAKE += "-DTARGET_PLATFORM=${TARGET_PLATFORM} \
                  -DUSE_USER_TMP_PATH=0 \
                  -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_PREFIX} \
                  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
                  -DCMAKE_INSTALL_INCLUDEDIR=${includedir} \
                  -DCMAKE_INSTALL_LIBDIR=${libdir} \
                  "

# AMBIORIX_BACKEND_PATH is not CMake variable so we have to pass it on compilation stage
#   Internal pair of double quotas is needed to define macro value as string
#   External pair of single quotas to protect internal ones from removing by shell on compilator call command
CXXFLAGS_append += "${@bb.utils.contains('PACKAGECONFIG', 'nbapi', '-DAMBIORIX_BACKEND_PATH=\'\\"/usr/bin/mods/amxb/mod-amxb-ubus.so\\"\'', '', d)}"

# FIXME: cmake correctly found full path of libnl3 library include dirs
# but in compiler cmdline it is present with cut prefix: -I/libnl3
# and so ignored as non-exist folder
CXXFLAGS_append_puma7 += " -v -I${STAGING_INCDIR}/libnl3"


def oecmake_var (name, d):
    v = d.getVar(name, True)
    return '-D{}={}'.format(name, v) if v else ''

EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_BRIDGE_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_BH_WIRE_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WLAN1_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WLAN2_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WLAN3_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_FILES_ENABLED', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_FILES_PATH', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_FILES_SUFFIX', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_FILES_AUTO_ROLL', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_STDOUT_ENABLED', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_LOG_SYSLOG_ENABLED', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_HOSTAP_WLAN1_CTRL_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_HOSTAP_WLAN2_CTRL_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_HOSTAP_WLAN3_CTRL_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WPA_SUPPLICANT_WLAN1_CTRL_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WPA_SUPPLICANT_WLAN2_CTRL_IFACE', d)}"
EXTRA_OECMAKE += "${@oecmake_var('BEEROCKS_WPA_SUPPLICANT_WLAN3_CTRL_IFACE', d)}"

do_configure_prepend() {
    install -d ${D}${datadir}/cmake/Modules
    install -m 0644 ${S}/cmake/Findnl-3.cmake ${D}${datadir}/cmake/Modules/
    install -m 0644 ${S}/cmake/Findnl-genl-3.cmake ${D}${datadir}/cmake/Modules/
}

do_install_append() {
    install -d ${D}${datadir}/cmake/Modules
    mv ${D}${INSTALL_PREFIX}/lib/cmake/* ${D}${datadir}/cmake/Modules/
    rmdir ${D}${INSTALL_PREFIX}/lib/cmake

    rm -rf ${D}${INSTALL_PREFIX}/host

    install_machine_specific
}
#
# Machine specific parameters
#

# Redefine next function for machine using override syntax
install_machine_specific() {
    bbnote "There are no machine specific files to install"
}

FILES_${PN} += "\
    ${INSTALL_PREFIX}/bin \
    ${INSTALL_PREFIX}/share \
    ${INSTALL_PREFIX}/lib \
    ${INSTALL_PREFIX}/scripts \
    ${INSTALL_PREFIX}/config \
"

FILES_${PN}-dev += "\
    ${INSTALL_PREFIX}/include \
    ${INSTALL_PREFIX}/lib/*.so \
    ${datadir}/cmake/Modules \
"

MACHINE_INCFILE[raspberrypi-rdk-broadband] = "raspberry-pi.inc"
MACHINE_INCFILE[turris] = "turris-omnia.inc"
MACHINE_INCFILE[puma7-atom] = "puma7-rdkb.inc"
MACHINE_INCFILE[cougarmountain] = "puma7-vanilla.inc"

require ${@d.getVarFlags('MACHINE_INCFILE').get(d.getVar('MACHINE', True), 'qemu.inc')}
#
# prplMesh startup
#
include ${@d.getVar('DISTRO', True) == 'rdk' and 'prplmesh-systemd.inc' or 'prplmesh-sysv.inc'}
