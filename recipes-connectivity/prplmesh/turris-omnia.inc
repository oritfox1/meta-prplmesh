# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
#
# Turris Omnia machine specific
#
SRC_URI_append_turris += " file://002-not-start-agent-wifi2.patch"

BEEROCKS_WLAN1_IFACE_turris = "wifi0"
BEEROCKS_WLAN2_IFACE_turris = "wifi1"
BEEROCKS_WLAN3_IFACE_turris = "wifi2"

BEEROCKS_HOSTAP_WLAN1_CTRL_IFACE_turris="/var/run/hostapd/wifi0"
BEEROCKS_HOSTAP_WLAN2_CTRL_IFACE_turris="/var/run/hostapd/wifi1"
BEEROCKS_HOSTAP_WLAN3_CTRL_IFACE_turris="/var/run/hostapd/wifi2"
