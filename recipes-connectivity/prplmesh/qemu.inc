# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.
#
# QEMU machine specific
#
BWL_TYPE_qemuarm ?= "bwl-dummy"
BWL_TYPE_qemuarm64 ?= "bwl-dummy"
BWL_TYPE_qemumips ?= "bwl-dummy"
BWL_TYPE_qemumips64 ?= "bwl-dummy"
BWL_TYPE_qemuppc ?= "bwl-dummy"
BWL_TYPE_qemux86 ?= "bwl-dummy"
BWL_TYPE_qemux86-64 ?= "bwl-dummy"
