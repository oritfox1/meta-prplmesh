# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020
# the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

# Changes in comparison with OpenWrt feed
# * no root password set
# * no MMX EP DB migrations
# * no UCI configs replacement
# * no permitted ifaces/IP/etc

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

DESCRIPTION = "MMX customer package for prplMesh"
HOMEPAGE = "https://github.com/InangoSystems/feed-mmx/tree/master/customers/inango-prplmesh-en"
LICENSE = "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM = "file://${S}/../../LICENSE;md5=3b43fbcd44ec443ceba0ba4ee19519fd"
SECTION = "mng"
DEPENDS = ""
RDEPENDS_${PN} = "lua5.1"

PV = "2.0.1"
SRC_URI = "\
    git://github.com/InangoSystems/feed-mmx.git;protocol=https;branch=mmx-as-luci-page \
    file://001-luapath-in-uci-defaults-script.patch \
"
SRCREV = "6f13f10efabb99128c604be9272007f71349b061"

S = "${WORKDIR}/git/customers/inango-prplmesh-en"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

LUAPATH ?= "${libdir}/lua/5.1"
FILES_${PN} += "\
    /www \
    /usr \
"

MMX_DEFAULTS_PATH = "${libdir}/mmx/mmx-defaults"
MMX_DEFAULTS_DB_PATH = "${MMX_DEFAULTS_PATH}/db"
MMX_DEFAULTS_DB_REFORMAT_PATH = "${MMX_DEFAULTS_PATH}/db_reformat"
MMX_DEFAULTS_CONF_PATH = "${MMX_DEFAULTS_PATH}/config/"
MMX_DEFAULTS_PASSWORD_FILE = "${MMX_DEFAULTS_PATH}/${PKG_NAME}_password"
MMX_DEFAULTS_PRODUCT_FILE = "${MMX_DEFAULTS_PATH}/product_info"
MMX_DEFAULTS_UPD_PRODUCT_FILE = "${MMX_DEFAULTS_PATH}/product_info_upd"

do_install() {
    # Presentation files processing
    install -d ${D}/www/luci-static/${PN}
    install -d ${D}/${LUAPATH}/luci/view/themes/${PN}

    # customer presentation files
    install -m 0755 ${S}/showfiles/cascade.css ${D}/www/luci-static/${PN}/cascade.css

    install -m 0755 ${S}/showfiles/inango_logo.svg ${D}/www/luci-static/${PN}/
    install -m 0755 ${S}/showfiles/powered-by-inango.svg ${D}/www/luci-static/${PN}/
    install -m 0755 ${S}/showfiles/prplmesh_logo.svg ${D}/www/luci-static/${PN}/

    install -m 0755 ${S}/showfiles/footer.htm ${D}/${LUAPATH}/luci/view/themes/${PN}/
    install -m 0755 ${S}/showfiles/header.htm ${D}/${LUAPATH}/luci/view/themes/${PN}/
    install -m 0755 ${S}/showfiles/header_login.htm ${D}/${LUAPATH}/luci/view/themes/${PN}/
    install -m 0755 ${S}/showfiles/sysauth.htm ${D}/${LUAPATH}/luci/view/themes/${PN}/

    install -d ${D}/www/luci-static/${PN}/fontawesome
    install -m 0644 ${S}/showfiles/fontawesome/* ${D}/www/luci-static/${PN}/fontawesome/.
    install -d ${D}/www/luci-static/${PN}/webfonts
    install -m 0644 ${S}/showfiles/webfonts/* ${D}/www/luci-static/${PN}/webfonts/.

    # MMX db files processing
    install -d ${D}/${MMX_DEFAULTS_DB_PATH}
    install -m 0755 ${S}/mmxdbfiles/db/mmx_meta_db ${D}/${MMX_DEFAULTS_DB_PATH}/mmx_meta_db
    install -m 0755 ${S}/mmxdbfiles/db/mmx_main_db ${D}/${MMX_DEFAULTS_DB_PATH}/mmx_main_db

    # FIXME: restore MMX EP database migrations (needed base scripts and luasqlite3 library)
    #install -d ${D}/${MMX_DEFAULTS_DB_REFORMAT_PATH}
    #install -m 0755 ${S}/mmxdbfiles/db_reformat ${D}/${MMX_DEFAULTS_DB_REFORMAT_PATH}

    # WEB files processing
    install -d ${D}/${LUAPATH}/luci/mmx
    install -m 0755 ${S}/frontend/WEB/*.lua ${D}/${LUAPATH}/luci/mmx/

    # CLI files processing
    install -d ${D}/usr/lib/mmxcli_xml
    install -m 0755 ${S}/frontend/CLI/*.xml ${D}/usr/lib/mmxcli_xml
    install -m 0755 ${S}/showfiles/startup.xml ${D}/usr/lib/mmxcli_xml

    # UCI-defaults init script processing
    install -d ${D}/${sysconfdir}/uci-defaults
    install -m 0755 ${S}/files/${PN}.init ${D}/${sysconfdir}/uci-defaults/luci-theme-openwrt-${PN}

    # Customer specific sysctl configuration
    install -d ${D}/${sysconfdir}/sysctl.d
    install -m 0644 ${S}/files/${PN}.sysctl ${D}/${sysconfdir}/sysctl.d/15-${PN}.conf

    ## UCI files processing is disabled
    #
    #install -d ${D}/etc/config
    #
    # install -d ${D}${MMX_DEFAULTS_CONF_PATH}
    # # - Processing network UCI file:
    # #   - Look for ${network_conffile} in conffiles folder and copy when present;
    # #     otherwise look for ${base_network_conffile} and copy it when present;
    # #     otherwise - do nothing {there is probably no network conffiles needed}.
    # if [ -f "${network_conffile}" ]; then \
    # install -m 0644 ${network_conffile} ${D}${MMX_DEFAULTS_CONF_PATH}/network ; \
    # elif [ -f "${base_network_conffile}" ]; then \
    # install -m 0644 ${base_network_conffile} ${D}${MMX_DEFAULTS_CONF_PATH}/network ; \
    # fi
    # # - Processing mmxbridge UCI file:
    # #   - Look for ${mmxbridge_conffile} in conffiles folder and copy when present;
    # #     otherwise look for ${base_mmxbridge_conffile} and copy it when present;
    # #     otherwise - do nothing {there is probably no network conffiles needed}.
    # if [ -f "${mmxbridge_conffile}" ]; then \
    # install -m 0644 ${mmxbridge_conffile} ${D}${MMX_DEFAULTS_CONF_PATH}/mmxbridge ; \
    # elif [ -f "${base_mmxbridge_conffile}" ]; then \
    # install -m 0644 ${base_mmxbridge_conffile} ${D}${MMX_DEFAULTS_CONF_PATH}/mmxbridge ; \
    # fi

    # # Processing the rest UCI files
    # for f in `ls ${S}/conffiles/`; do if [ "$$$${f:0:7}" != "network" -a "$$$${f:0:9}" != "mmxbridge" ]; then install -m 0644 ${S}/conffiles/$$$$f ${D}/${MMX_DEFAULTS_CONF_PATH}/. ; fi ; done

    # Product info file processing
    install -m 0644 ${S}/files/*product_info ${D}/${MMX_DEFAULTS_PRODUCT_FILE}
    install -m 0755 ${S}/files/*product_info_upd ${D}/${MMX_DEFAULTS_UPD_PRODUCT_FILE}

    # opkg is absent - so non relevant
    # # Install keep file
    # install -d ${D}/lib/upgrade/keep.d/
    # install -m 0644 ${S}/files/${PN}.keep  ${D}/lib/upgrade/keep.d/${PN}.keep

    ## We don't need feature "Permitted MAC/ifaces" in prplMesh customer package
    ## Install SOURCE files
    ## Permitted interfaces libraries {LUA modules}
    ## install -d ${D}/${LUAPATH}/mmx
    ##  Permitted IP interfaces
    #install -m 0755 ${S}/src/mmx_ip_permifaces.lua ${D}/${LUAPATH}/mmx/
    ##  -------------------------------------------------------------
    ##  Permitted Ethernet interfaces {per platform adjustment with a special conffile}
    #install -m 0755 ${S}/src/mmx_eth_permifaces.lua ${D}/${LUAPATH}/mmx/
    ##  platform_name is defined above in this file
    #tmp=$$$${grep ^${platform_name}= ${S}/src/mmx_eth_permifaces.config | cut -d= -f2-} ; \
    #sed -i 's/^PLATFORM_ETH_PERMIFACES = \"\".*/PLATFORM_ETH_PERMIFACES = \"'"$$$$tmp"'\"/g' \
    #${D}/${LUAPATH}/mmx/mmx_eth_permifaces.lua
    ##  -------------------------------------------------------------
    ##  Permitted MAC Filter interfaces
    ##   TODO
    ##  -------------------------------------------------------------
    ##  Permitted Bridge/Bridge port interfaces
    ##  -------------------------------------------------------------
    #install -m 0755 ${S}/src/mmx_bridge_permifaces.lua ${D}/${LUAPATH}/mmx/

    # Set the product SW version according to this package version
    sed -i 's/^SoftwareVersion=.*/SoftwareVersion=${PV}/g' ${D}/${MMX_DEFAULTS_PRODUCT_FILE}
    sed -i 's/^SoftwareBuildDate=.*/SoftwareBuildDate=2021/g' ${D}/${MMX_DEFAULTS_PRODUCT_FILE}
}
