# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

DESCRIPTION = "MMX Backend for prplMesh Ambiorix API"
HOMEPAGE = "https://github.com/InangoSystems/prplmesh-be"
LICENSE = "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b43fbcd44ec443ceba0ba4ee19519fd"
SECTION = "utils"
RDEPENDS_${PN} = "lua5.1 lua-socket"

SRC_URI = "git://github.com/InangoSystems/prplmesh-be.git;protocol=https"
SRCREV = "aca8c81a86f0873e43612c517cc07b840526ee41"

S = "${WORKDIR}/git"

LUAPATH ?= "${libdir}/lua/5.1"
FILES_${PN} += "${LUAPATH}"

do_install() {
    oe_runmake install DESTDIR=${D} LUAPATH=${LUAPATH}
}
