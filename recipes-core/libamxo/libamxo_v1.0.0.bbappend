# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: 2020 the prplMesh contributors (see AUTHORS.md)
# This code is subject to the terms of the BSD+Patent license.
# See LICENSE file for more details.

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI_append += " file://001-disable-werror.patch"
